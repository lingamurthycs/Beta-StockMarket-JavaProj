CREATE DATABASE  IF NOT EXISTS `springbootdb` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `springbootdb`;
-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: springbootdb
-- ------------------------------------------------------
-- Server version	5.7.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `name` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-06 20:27:53


CREATE TABLE `MyTollRNMS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rnmsno` int(11) DEFAULT NULL,
  `status` varchar(255),
  PRIMARY KEY (`id`)
) ;



insert into mytollrnms values(1,4001,'unused');
insert into mytollrnms values(2,4002,'unused');
insert into mytollrnms values(3,4003,'unused');
insert into mytollrnms values(4,4004,'unused');
insert into mytollrnms values(5,4005,'unused');
insert into mytollrnms values(6,4006,'unused');
insert into mytollrnms values(7,4007,'unused');
insert into mytollrnms values(8,4008,'unused');
insert into mytollrnms values(9,4009,'unused');
insert into mytollrnms values(10,4010,'unused');

insert into mytollrnms values(11,4011,'unused');
insert into mytollrnms values(12,4012,'unused');
insert into mytollrnms values(13,4013,'unused');
insert into mytollrnms values(14,4014,'unused');
insert into mytollrnms values(15,4015,'unused');
insert into mytollrnms values(16,4016,'unused');
insert into mytollrnms values(17,4017,'unused');
insert into mytollrnms values(18,4018,'unused');
insert into mytollrnms values(19,4019,'unused');
insert into mytollrnms values(20,4020,'unused');
insert into mytollrnms values(21,4021,'unused');
insert into mytollrnms values(22,4022,'unused');
insert into mytollrnms values(23,4023,'unused');
insert into mytollrnms values(24,4024,'unused');

insert into mytollrnms values(25,4025,'unused');
insert into mytollrnms values(26,4026,'unused');
insert into mytollrnms values(27,4027,'unused');
insert into mytollrnms values(28,4028,'unused');