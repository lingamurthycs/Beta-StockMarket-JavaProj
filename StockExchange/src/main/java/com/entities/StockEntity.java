package com.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "stockDetails")
@NamedQuery(name = "StockEntity.findAll", query = "SELECT d FROM StockEntity d WHERE d.userName = 'userName'")
public class StockEntity implements Serializable{

	/**
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	public String ID;
	
	@JsonProperty("userName")
	@Column(name = "userName")
	public String userName;
	
	@Column(name = "timeOfTrade")
	public Date timeOfTrade;

	@JsonProperty("stockName")
	@Column(name = "stockName")
	public String stockName;

	@JsonProperty("stockPrice")
	@Column(name = "stockPrice")
	public double stockPrice;

	@JsonProperty("volumePurchased")
	@Column(name = "volumePurchased")
	public double volumePurchased;
	
	@JsonProperty("totalStockPurchasePrice")
	@Column(name = "totalStockPurchasePrice")
	public double totalStockPurchasePrice;

	@JsonProperty("fees")
	@Column(name = "fees")
	public double fees;

	@JsonProperty("totalIncludingFees")
	@Column(name = "totalIncludingFees")
	public double totalIncludingFees;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the timeOfTrade
	 */
	public Date getTimeOfTrade() {
		return timeOfTrade;
	}

	/**
	 * @param timeOfTrade the timeOfTrade to set
	 */
	public void setTimeOfTrade(Date timeOfTrade) {
		this.timeOfTrade = timeOfTrade;
	}

	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * @param stockName the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	/**
	 * @return the stockPrice
	 */
	public double getStockPrice() {
		return stockPrice;
	}

	/**
	 * @param stockPrice the stockPrice to set
	 */
	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}

	/**
	 * @return the volumePurchased
	 */
	public double getVolumePurchased() {
		return volumePurchased;
	}

	/**
	 * @param volumePurchased the volumePurchased to set
	 */
	public void setVolumePurchased(double volumePurchased) {
		this.volumePurchased = volumePurchased;
	}

	/**
	 * @return the totalStockPurchasePrice
	 */
	public double getTotalStockPurchasePrice() {
		return totalStockPurchasePrice;
	}

	/**
	 * @param totalStockPurchasePrice the totalStockPurchasePrice to set
	 */
	public void setTotalStockPurchasePrice(double totalStockPurchasePrice) {
		this.totalStockPurchasePrice = totalStockPurchasePrice;
	}

	/**
	 * @return the fees
	 */
	public double getFees() {
		return fees;
	}

	/**
	 * @param fees the fees to set
	 */
	public void setFees(double fees) {
		this.fees = fees;
	}

	/**
	 * @return the totalIncludingFees
	 */
	public double getTotalIncludingFees() {
		return totalIncludingFees;
	}

	/**
	 * @param totalIncludingFees the totalIncludingFees to set
	 */
	public void setTotalIncludingFees(double totalIncludingFees) {
		this.totalIncludingFees = totalIncludingFees;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StockEntity [userName=" + userName + ", timeOfTrade=" + timeOfTrade + ", stockName=" + stockName
				+ ", stockPrice=" + stockPrice + ", volumePurchased=" + volumePurchased + ", totalStockPurchasePrice="
				+ totalStockPurchasePrice + ", fees=" + fees + ", totalIncludingFees=" + totalIncludingFees + "]";
	}

}
