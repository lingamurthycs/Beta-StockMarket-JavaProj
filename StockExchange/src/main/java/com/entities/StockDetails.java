package com.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

/**
 * Get the list of StockDetails.
 */
@ApiModel(description = "Get the list of images for the provided document number")
@javax.annotation.Generated(value = "com.ensat.stock", date = "2018-01-24T01:36:37.011Z")

public class StockDetails {

	/** The image count. */
	@JsonProperty("listStock")
	private List<Stock> listStock = null;

	/**
	 * @return the listStock
	 */
	public List<Stock> getListStock() {
		return listStock;
	}

	/**
	 * @param listStock
	 *            the listStock to set
	 */
	public void setListStock(List<Stock> listStock) {
		this.listStock = listStock;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "StockDetails [listStock=" + listStock + "]";
	}

}
