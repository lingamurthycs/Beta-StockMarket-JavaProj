package com.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

/**
 * Contains the binary images value.
 */
@ApiModel(description = "Contains stock details")
@javax.annotation.Generated(value = "com.ensat.entities.Stock", date = "2018-01-20T12:44:10.738Z")

public class FeesDetails {

	public FeesDetails(){
		
	}
	
	/** The value. */
	@JsonProperty("name")
	private String stockName = null;

	/** The name. */
	@JsonProperty("price")
	private double stockPrice;

	/**
	 * @return the stockName
	 */
	public String getStockName() {
		return stockName;
	}

	/**
	 * @param stockName the stockName to set
	 */
	public void setStockName(String stockName) {
		this.stockName = stockName;
	}

	/**
	 * @return the stockPrice
	 */
	public double getStockPrice() {
		return stockPrice;
	}

	/**
	 * @param stockPrice the stockPrice to set
	 */
	public void setStockPrice(double stockPrice) {
		this.stockPrice = stockPrice;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Stock [stockName=" + stockName + ", stockPrice=" + stockPrice + "]";
	}

}
