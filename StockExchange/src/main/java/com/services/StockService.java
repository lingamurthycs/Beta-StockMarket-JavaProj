package com.services;

import java.util.List;

import com.entities.StockDetails;
import com.entities.StockEntity;

public interface StockService {

	public StockDetails getStockDetails() throws Exception;
	public void addStockDetails(StockEntity stock) throws Exception;
	public List<StockEntity> getUserStockDetails(String userName) throws Exception;
	
}
