package com.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.controllers.StockApiController;
import com.entities.Stock;
import com.entities.StockDetails;
import com.entities.StockEntity;
import com.repositories.StockDao;
import com.util.Utility;

@Service
public class StockServiceImpl implements StockService {

	/** The logger. */
	private static Logger logger = Logger.getLogger(StockApiController.class);

	@Autowired
	private StockDao stockDao;

	/*
	 * (non-Javadoc) getStockDetails
	 */
	@Override
	public StockDetails getStockDetails() throws Exception {
		logger.info("StockServiceImpl :: Start");
		List<Stock> listStock = new ArrayList<Stock>();

		Stock stock = new Stock();
		stock.setStockName("Aditya  Birla");
		stock.setStockPrice(Utility.randomNumber());

		Stock stock1 = new Stock();
		stock1.setStockName("Ing bank");
		stock1.setStockPrice(Utility.randomNumber());

		Stock stock2 = new Stock();
		stock2.setStockName("Reliance");
		stock2.setStockPrice(Utility.randomNumber());

		Stock stock3 = new Stock();
		stock3.setStockName("HCL");
		stock3.setStockPrice(Utility.randomNumber());

		Stock stock4 = new Stock();
		stock4.setStockName("IBM");
		stock4.setStockPrice(Utility.randomNumber());

		listStock.add(stock);
		listStock.add(stock1);
		listStock.add(stock2);
		listStock.add(stock3);
		listStock.add(stock4);

		StockDetails stockDetails = new StockDetails();
		stockDetails.setListStock(listStock);

		return stockDetails;
	}

	/*
	 * (non-Javadoc) addStockDetails will add data in DB
	 */
	@Override
	public void addStockDetails(StockEntity stock) throws Exception {
		logger.info("StockServiceImpl :: addStockDetails ::: Start" + stock);
		stock.setTimeOfTrade(new Date());
		stockDao.saveAndFlush(stock);
	}

	@Override
	public List<StockEntity> getUserStockDetails(String userName) throws Exception {
		List<StockEntity> userDetails=stockDao.findByUserName(userName);
		return userDetails;
	}

}
