package com.repositories;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.entities.StockEntity;

@Repository
public interface StockDao extends JpaRepository<StockEntity,BigInteger>, JpaSpecificationExecutor<StockEntity> {

	List<StockEntity> findByUserName(String userName);
}
