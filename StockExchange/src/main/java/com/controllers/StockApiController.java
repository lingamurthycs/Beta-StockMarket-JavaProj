package com.controllers;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.entities.StockDetails;
import com.entities.StockEntity;
import com.services.StockService;

import io.swagger.annotations.ApiParam;

/**
 * The Class StockApiController.
 */
@javax.annotation.Generated(value = "com.controllers.Stock", date = "2018-01-20T12:44:10.738Z")

@Controller
@CrossOrigin
public class StockApiController implements StockApi {
	
	/** The logger. */
	private static Logger logger = Logger.getLogger(StockApiController.class);

	@Autowired
	StockService stockService;
	
    /* (non-Javadoc)
     * getStockDetails will get the stock information like name and price
     * 
     */
    public ResponseEntity<StockDetails> getStockDetails(
        @ApiParam(value = "user name.") @RequestParam(value = "userName", required = false) String userName
        ) {
       
     	logger.info("StockApiController Request" + userName);

     	StockDetails stockDetails =null;
    	try {
    		stockDetails=stockService.getStockDetails();
			return new ResponseEntity<StockDetails>(stockDetails, HttpStatus.OK);
		}  catch (Exception exp) {
			logger.error("Some internal error found", exp);
			return new ResponseEntity<StockDetails>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    /* (non-Javadoc)
     * addStockDetails will add details in DB
     * 
     */
	@Override
	public ResponseEntity<StockEntity> addStockDetails(@Valid @RequestBody StockEntity body) {
		logger.info("addStockDetails :: StockEntity : " +body);
		try {
			stockService.addStockDetails(body);
		} catch (Exception e) {
			logger.error("SaddStockDetails : some internal error found", e);
			return new ResponseEntity<StockEntity>(HttpStatus.INTERNAL_SERVER_ERROR);

		}
		return null;
	}

	@Override
	public ResponseEntity<List<StockEntity>> getUserStockDetais(String userName) {
		logger.info("getUserStockDetais :: userName : " +userName);
		List<StockEntity> userDetails=null;
		try {
			userDetails=stockService.getUserStockDetails(userName);
		} catch (Exception e) {
			logger.error("getUserStockDetais :: Some internal error found", e);
			return new ResponseEntity<List<StockEntity>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<List<StockEntity>>(userDetails, HttpStatus.OK);
	}

}
