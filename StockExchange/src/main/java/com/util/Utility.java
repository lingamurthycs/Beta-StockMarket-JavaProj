package com.util;

import java.util.Random;

import org.apache.log4j.Logger;

import com.controllers.StockApiController;


public class Utility {
	  
   private Utility(){
		
	}
    
   /** The logger. */
	private static Logger logger = Logger.getLogger(StockApiController.class);

	public static double randomNumber() {

		int randomInt = 0;
	    Random rn = new Random();
	    randomInt = rn.nextInt(42) + 1;
	    double b = randomInt;
	    logger.info("RandomNumber " +b);
		return b;

	}
    

}
